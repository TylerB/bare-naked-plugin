# Bare Naked Wordpress Plugin (depends on ACF Pro)
Disable WordPress bloat features and add useful features.

This plugin adds an options page with the following options:
  - Disable jQuery Migrate
  - Disable WP Emojis
  - Disable REST API
  - Disable wp-embed.js
  - Disable XML-RPC
  - Disable Windows Live Writer support
  - Remove WordPress public version from site head

The plugin also adds the following features:
  - Custom wp-login logo and background color
  - Allow SVG Upload


## Instructions
  - Install ACF Pro (https://www.advancedcustomfields.com/)  
  - Add any plugins you want to automatically install to the /Bare WP/install_plugins/ directory
  - Zip up the folder "Bare WP" and upload it to your WordPress site.
  - Navigate to the Bare WP options page from your wp-admin area
  - Configure your settings
