<?php
if( function_exists('acf_add_options_page') ):

  acf_add_options_page(array(
    'page_title' 	=> 'Bare WordPress',
    'menu_title'	=> 'Bare WP',
    'menu_slug' 	=> 'bare-wp',
    'capability'	=> 'edit_posts',
    'icon_url' => 'dashicons-editor-code',
    'position' => 2,
    'redirect'		=> false
  ));

endif;


if( function_exists('acf_add_local_field_group')):


  acf_add_local_field_group(array(
    'key' => 'login_page_customization',
    'title' => 'Login page customization',
    'fields' => array (
      array (
        'key' => 'wp_logo',
        'label' => 'Logo',
        'instructions' => 'Upload a logo to replace the W logo.',
        'name' => 'wp_logo',
        'type' => 'image',
        'return_format' => 'url',
      ),
      array (
        'key' => 'login_logo_height',
        'label' => 'Login Logo Height',
        'name' => 'login_logo_height',
        'type' => 'number',
      ),
      array (
        'key' => 'login_bg_color',
        'label' => 'Page Background Color',
        'name' => 'login_bg_color',
        'type' => 'color_picker',
      ),
      array (
        'key' => 'login_form_bg_color',
        'label' => 'Form Background Color',
        'name' => 'login_form_bg_color',
        'type' => 'color_picker',
      ),
      array (
        'key' => 'font_color',
        'label' => 'Font Color',
        'name' => 'font_color',
        'type' => 'color_picker',
      ),
      array (
        'key' => 'link_color',
        'label' => 'Link Color',
        'name' => 'link_color',
        'type' => 'color_picker',
      ),
      array (
        'key' => 'login_button_color',
        'label' => 'Button Colour',
        'name' => 'login_button_color',
        'type' => 'color_picker',
      ),
      array (
        'key' => 'login_button_text_color',
        'label' => 'Button Text Colour',
        'name' => 'login_button_text_color',
        'type' => 'color_picker',
      )
    ),
    'location' => array (
      array (
        array (
          'param' => 'options_page',
          'operator' => '==',
          'value' => 'bare-wp',
        ),
      ),
    ),
    'menu_order' => 0
  ));


  acf_add_local_field_group(array(
    'key' => 'group_2',
    'title' => 'Features',
    'fields' => array (
      array (
        'key' => 'svg_upload',
        'label' => 'SVG Upload',
        'instructions' => 'Allow SVG upload through the media library',
        'name' => 'svg_upload',
        'type' => 'true_false',
        'message' => 'Activate'
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'options_page',
          'operator' => '==',
          'value' => 'bare-wp',
        ),
      ),
    ),
    'menu_order' => 1
  ));

  acf_add_local_field_group(array(
    'key' => 'group_1',
    'title' => 'Bloat',
    'fields' => array (
      array (
        'key' => 'jquery_migrate',
        'label' => 'jQuery Migrate',
        'instructions' => 'This is used to warn about and/or restore APIs and behaviors that have been deprecated and/or removed as of jQuery version 3.0. If your plugins and scripts are up to date, it can be deactivated.',
        'name' => 'jquery_migrate',
        'type' => 'true_false',
        'message' => 'Deactivate'
      ),
      array (
        'key' => 'wp_emojis',
        'label' => 'WP Emojis',
        'instructions' => "If your Writer is older than 14 this can safely be deactivated.",
        'name' => 'wp_emojis',
        'type' => 'true_false',
        'message' => 'Deactivate'
      ),
      array (
        'key' => 'rest_api',
        'label' => 'REST API',
        'instructions' => 'The WP (JSON) REST API is a connector between WordPress and other software applications which is characterized by universality and high compatibility. Its generally used to develop WordPress sites with javascript or other languages besides PHP. This may break plugins if they rely on this API.',
        'name' => 'rest_api',
        'type' => 'true_false',
        'message' => 'Deactivate'
      ),
      array (
        'key' => 'wp_embed',
        'label' => 'wp-embed.js',
        'instructions' => "Embeds: Remove & characters from the inline embed JS.
  Older versions of WordPress will convert those & characters to & #038;, which makes for some non-functional JS. If WordPress is up to date, this can safely be deactivated.",
        'name' => 'wp_embed',
        'type' => 'true_false',
        'message' => 'Deactivate'
      ),
      array (
        'key' => 'xml_rpc',
        'label' => 'XML-RPC',
        'instructions' => '<a href="https://www.wordfence.com/blog/2015/10/should-you-disable-xml-rpc-on-wordpress/" target="_blank">Should you disable XML-RPC?</a>',
        'name' => 'xml_rpc',
        'type' => 'true_false',
        'message' => 'Deactivate'
      ),
      array (
        'key' => 'windows_livewriter',
        'label' => 'Windows Live Writer',
        'instructions' => "If <a href='https://wordpress.com/windows-live-writer/' target='_blank'>Windows Live Writer</a> is not being used, this can be deactivated.",
        'name' => 'windows_livewriter',
        'type' => 'true_false',
        'message' => 'Deactivate'
      ),
      array (
        'key' => 'wordpress_version',
        'label' => 'Public WordPress Version',
        'instructions' => 'Disable the WP version from being printed to the head. The only reason to not deactivate this is if you want to allow hackers to know your WP version is outdated.',
        'name' => 'wordpress_version',
        'type' => 'true_false',
        'message' => 'Deactivate'
      )
    ),
    'location' => array (
      array (
        array (
          'param' => 'options_page',
          'operator' => '==',
          'value' => 'bare-wp',
        ),
      ),
    ),
    'menu_order' => 2
  ));

endif;
 ?>
