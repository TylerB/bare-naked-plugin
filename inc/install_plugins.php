<?php
$plugin_dir_path = realpath(__DIR__ . '/..') . '/install_plugins/';

if (file_exists($plugin_dir_path)) {
  $plugins_to_install = preg_grep('~\.zip$~', scandir($plugin_dir_path, 1));

  if (count($plugins_to_install) > 0) {
    $installed_plugins = array_diff(scandir(WP_PLUGIN_DIR, 1), array('..', '.', '.DS_Store'));
    foreach ($plugins_to_install as $plugin) {

      $plugin_minus_zip = str_replace('.zip', '', $plugin );
      $plugin_already_installed = false;

      foreach($installed_plugins as $installed_plugin) {
        if ($installed_plugin == $plugin_minus_zip) {
          $plugin_already_installed = true;
          break;
        }
      }

      $zip = new ZipArchive;
      $res = $zip->open($plugin_dir_path . $plugin);
      if ($res === TRUE && !$plugin_already_installed) {
          $zip->extractTo(WP_PLUGIN_DIR);
          $zip->close();
          unlink($plugin_dir_path . $plugin);
      }
    }
  }
}
?>
