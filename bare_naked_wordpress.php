<?php
    /*
    Plugin Name: Bare Naked Wordpress
    Plugin URI: https://github.com/tyler-barnes/bare-wp
    Description: Remove bulky WordPress features and add useful ones (relies on ACF Pro)
    Author: Tyler Barnes
    Version: 1.2
    Author URI: http://www.tylerbarnes.ca
    */



if (is_admin()) {
  include_once('inc/install_plugins.php');
}

add_action('plugins_loaded', 'ACFOptionsPage');

function ACFOptionsPage() {

  if ( class_exists( 'acf' )) {

        if (is_admin()) {
          include_once('inc/options_page.php');
        }

        //wp logo upload
        if( get_field( 'wp_logo', 'options' ) ):
          function my_login_logo() { ?>
              <style type="text/css">
                  * {
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                  }

                  body.login {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                  }

                  .wp-core-ui .button-primary, .wp-core-ui .button-primary.focus, .wp-core-ui .button-primary.hover, .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover {
                      border: none !important;
                      border-radius: 0 !important;
                      letter-spacing: 1px !important;
                      outline: 0 !important;
                      -webkit-box-shadow: none !important;
                              box-shadow: none !important;
                      text-shadow: none !important;
                      <?php if( get_field( 'login_button_color', 'options' ) ): ?>
                        background-color: <?php the_field( 'login_button_color', 'options' ); ?> !important;
                      <?php endif; ?>
                      <?php if( get_field( 'login_button_text_color', 'options' ) ): ?>
                        color: <?php the_field( 'login_button_text_color', 'options' ); ?> !important;
                      <?php endif; ?>
                  }

                  #login {
                    padding-top: 2% !important;
                  }

                  #login h1 a, .login h1 a {
                    <?php if ( get_field('wp_logo', 'options') ): ?>
                      <?php $logo = get_field('wp_logo', 'options') ;?>
                      background-image: url(<?php echo wp_get_attachment_url($logo); ?>);
                    <?php endif; ?>
                		<?php if( get_field( 'login_logo_height', 'options' ) ): ?>
                      height: <?php the_field( 'login_logo_height', 'options' ); ?>px;
                    <?php else: ?>
                      height:180px;
                    <?php endif; ?>
                		width: 400px;
                    max-width: 100%;
                		background-size: contain;
                		background-repeat: no-repeat;
                    background-position: center;
                  	padding-bottom: 30px;
                  }

                <?php if (get_field('login_form_bg_color', 'options')): ?>
                  .login form {
                    background-color: <?php the_field('login_form_bg_color', 'options'); ?> !important;
                  }
                <?php endif; ?>

                <?php if (get_field('font_color', 'options')): ?>
                  .login label {
                    color: <?php the_field('font_color', 'options'); ?> !important;
                  }
                <?php endif; ?>

                <?php if( get_field( 'login_bg_color', 'options' ) ): ?>
                  body {
                    background-color: <?php the_field( 'login_bg_color', 'options' ); ?> !important;
                  }
                <?php endif; ?>

                <?php if( get_field( 'link_color', 'options' ) ): ?>
                  .login #backtoblog a, .login #nav a {
                    color: <?php the_field( 'link_color', 'options' ); ?> !important;
                  }
                <?php endif; ?>
              </style>
          <?php }
          add_action( 'login_enqueue_scripts', 'my_login_logo' );
        endif;





        //allow svg upload
        if( get_field( 'svg_upload', 'options' ) ):
          function cc_mime_types($mimes) {
            $mimes['svg'] = 'image/svg+xml';
            return $mimes;
          }
          add_filter('upload_mimes', 'cc_mime_types');
        endif;




        //remove jquery migrate
        if( get_field( 'jquery_migrate', 'options' ) ):
          add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );

          function dequeue_jquery_migrate( &$scripts){
          	if(!is_admin()){
          		$scripts->remove( 'jquery');
          		$scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
          	}
          }
        endif;




        //remove emojis
        if( get_field( 'wp_emojis', 'options' ) ):
          function disable_wp_emojicons() {

            // all actions related to emojis
            remove_action( 'admin_print_styles', 'print_emoji_styles' );
            remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
            remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
            remove_action( 'wp_print_styles', 'print_emoji_styles' );
            remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
            remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
            remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

            function disable_emojicons_tinymce( $plugins ) {
              if ( is_array( $plugins ) ) {
                return array_diff( $plugins, array( 'wpemoji' ) );
              } else {
                return array();
              }
            }

            // filter to remove TinyMCE emojis
            add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
          }
          add_action( 'init', 'disable_wp_emojicons' );
        endif;






        //remove rest api links
        if( get_field( 'rest_api', 'options' ) ):

          // Remove REST API info from head and headers
          remove_action( 'wp_head',      'wp_oembed_add_discovery_links'         );
          remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
          remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
          remove_action( 'template_redirect', 'rest_output_link_header', 11 );

          // WordPress 4.7+ disables the REST API via authentication short-circuit.
          // For versions of WordPress < 4.7, disable the REST API via filters
          if ( version_compare( get_bloginfo( 'version' ), '4.7', '>=' ) ) {
          	require_once( plugin_dir_path( __FILE__ ) . 'classes/disable-rest-api.php' );
          	new Disable_REST_API( __FILE__ );
          } else {
          	require_once( plugin_dir_path( __FILE__ ) . 'functions/legacy.php' );
          	DRA_Disable_Via_Filters();
          }
        endif;




        //remove wp embed
        if( get_field( 'wp_embed', 'options' ) ):
          function my_deregister_scripts(){
            wp_deregister_script( 'wp-embed' );
          }
          add_action( 'wp_footer', 'my_deregister_scripts' );
        endif;





        // Disable use XML-RPC
        if( get_field( 'xml_rpc', 'options' ) ):
          function remove_x_pingback($headers) {
              unset($headers['X-Pingback']);
              return $headers;
          }
          add_filter('wp_headers', 'remove_x_pingback');
          add_filter('xmlrpc_enabled', '__return_false');
          remove_action ('wp_head', 'rsd_link');
        endif;




        //remove windows live writer
        if( get_field( 'windows_livewriter', 'options' ) ):
          remove_action( 'wp_head', 'wlwmanifest_link');
        endif;





        //remove wp version from header
        if( get_field( 'wordpress_version', 'options' ) ):
          remove_action('wp_head', 'wp_generator');
        endif;

  }

}

?>
